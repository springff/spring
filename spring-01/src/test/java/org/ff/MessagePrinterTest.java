package org.ff;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MessagePrinterTest {
    @Test
    public void printTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        MessagePrinter printer = context.getBean(MessagePrinter.class);
        printer.messagePrinter(); // 实例化成功，方法调用成功
        printer.printMessage(); // 持有的依赖对象也成功被 di (dependency inject)，调用持有对象的方法也成功
    }
}
