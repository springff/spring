# Spring 入门

idea spring 入门依赖注入，idea 创建空项目添加多个 moudel

参考：[Spring学习(1)——快速入门 - 我没有三颗心脏 - 博客园 (cnblogs.com)](https://www.cnblogs.com/wmyskxz/p/8820371.html)

## spring-01 依赖注入 

依赖注入理解：不通过 new Object() 的方式获取对象的实例，只需要声明对象，对象的实例化通过 spring 框架自动实例化后注入，也就是 dependency inject （依赖注入）

### 思路

- 添加 spring 核心依赖
- 在 spring 配置文件中设置实例化对象的扫描包路径，指定那些类能够被自动实例化
- 创建实例化对象，使用注解标明类，表示将通过 spring 依赖注入的方式获取类的实例对象
- 在测试方法中，手动获取 spring 上下文对象，通过上下文对象获取类的实例对象，调用实例对象的方法

关键字：

@Component 声明是可以被 spring 自动实例化的类

@Autowired 表示类的属性的实例将通过 spring 进行插入

### 详细步骤

添加 spring 核心依赖

```xml
<dependencies>
    <!--spring 核心依赖-->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>5.3.3</version>
    </dependency>
    <!-- 测试框架 -->
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

在 spring 配置文件`resouces\applicationContext.xml`中设置实例化对象的扫描包路径 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/context/spring-context.xsd">
    <!-- 配置实例扫描的包路径，开发过程中也有将整个工程目录配置为扫描的路径 -->
    <context:component-scan base-package="org.ff"/>
</beans>
```

创建将要被自动实例化的类，该类使用 @Component 注解

```java
package org.ff;

import org.springframework.stereotype.Component;

@Component
public class MessageService {
    public void MessageService(){
        System.out.println("MessageService....");
    }

    public String getMessage(){
        return "Hello World";
    }
}
```

MessagePrinter 持有 MessageService 的依赖，通过为 messageService 属性添加 set 方法，并为该方法添加 @Autowired 注解，表示在自动实例化 MessagePrinter  的时候也会自动获取 messageService 的实例。

```java
package org.ff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessagePrinter {
    private MessageService messageService;

    @Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void messagePrinter(){
        System.out.println("MessagePrinter...");
    }

    public void printMessage(){
        System.out.println("printer: " + messageService.getMessage());
    }
}
```

读取 spring 配置文件获取 context 上下文对象，获取对应类的实例对象。

```java
package org.ff;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MessagePrinterTest {
    @Test
    public void printTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        MessagePrinter printer = context.getBean(MessagePrinter.class);
        printer.messagePrinter(); // 实例化成功，方法调用成功
        printer.printMessage(); // 持有的依赖对象也成功被 di (dependency inject)，调用持有对象的方法也成功
    }
}
```

运行结果

![image-20210725153256778](assets/README/image-20210725153256778.png)



## spring-02 idea 添加 moduel

idea 空项目 + 多 moduel 工程结构的创建与还原；idea 工程多个 module，并且多个module 位于工程的一级结构。

### 创建工程

**效果展示：**

工程结构

<img src="assets/README/image-20210817170817099.png" alt="image-20210817170817099" style="zoom: 80%;" />

目录结构

<img src="assets/README/image-20210817170902289.png" alt="image-20210817170902289" style="zoom:80%;" />

**步骤：**

**1，idea 创建空项目**

<img src="assets/README/image-20210817171700963.png" alt="image-20210817171700963" style="zoom: 50%;" />

**2，创建空项目之后添加 module**

<img src="assets/README/image-20210817172100436.png" alt="image-20210817172100436"  />

![image-20210817172244179](assets/README/image-20210817172244179.png)

**3，git 上传忽略工程文件**

```
# 忽略自身
.gitignore
# 编译目录
target/
# idea 缓存文件
/.idea/
*.iml
```

注意：创建空项目之后不添加 module，会将空项目创建为默认的 module，之后添加的 module 都会放在默认的 module 目录下。如果出现该情况，需要先将默认 module 删除再添加新的 module。

### 还原工程

github/gitee 上clone下的工程由于没有工程文件导入之后会找不到 module，需要修改一下 project structure

clone 下来的项目

<img src="assets/README/image-20210817172722477.png" alt="image-20210817172722477" style="zoom:67%;" />

idea 导入之后工程结构（不符合预期）

<img src="assets/README/image-20210817172844070.png" alt="image-20210817172844070" style="zoom:67%;" />

**步骤：**

1，将自动创建的默认 module 删除

![image-20210817173122848](assets/README/image-20210817173122848.png)

2，将 module 依次导入

![image-20210817173038087](assets/README/image-20210817173038087.png)

3，导入 module 的过程中选择 module 的创建模板（根据 moduel 原本创建的模板选择）

<img src="assets/README/image-20210817173249700.png" alt="image-20210817173249700" style="zoom: 80%;" />

4，导入之后工程结构就恢复正常

<img src="assets/README/image-20210817173504299.png" alt="image-20210817173504299" style="zoom:80%;" />