package org.ff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessagePrinter {
    private MessageService messageService;

    @Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void messagePrinter(){
        System.out.println("MessagePrinter...");
    }

    public void printMessage(){
        System.out.println("printer: " + messageService.getMessage());
    }
}
